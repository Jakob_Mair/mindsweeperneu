/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mindsweeperjakob_mair;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Queue;
import java.util.Random;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.*;

/**
 *
 * @author Jakob
 */
public class MSGUI extends JFrame implements MouseListener {

    private JPanel mainPanel = null;

    private mindsweeperbuttonn[][] Msfelder;

    //Variablen
    public MSGUI() {
        initialiseGui();
        einfügenZahlen();

        placeBombs();
        einfügenZahlen();
        unCoverButton();

    }

    private void initialiseGui() {

        //JPanel Grundeinstellungen
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());
        this.setContentPane(mainPanel);

        this.setSize(800, 800);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setResizable(false);

        //Intitialisieren Variablen
        Msfelder = new mindsweeperbuttonn[10][10];

        //Feld generierung (Schleife)
        for (int z = 0; z < 10; z++) {

            for (int s = 0; s < 10; s++) {

                Msfelder[z][s] = new mindsweeperbuttonn(0, z, s);

                Msfelder[z][s].setSize(50, 50);
                GridBagConstraints con = new GridBagConstraints();
                con.gridx = s; // 
                con.gridy = z; //
                con.fill = GridBagConstraints.BOTH; //Rand ausfüllen
                con.weightx = 1; //
                con.weighty = 1; //
                mainPanel.add(Msfelder[z][s], con);  //FeldButtonsAdden
                Msfelder[z][s].setText(" ");
                //Listener
                Msfelder[z][s].addMouseListener(this);
            }

        }

        //Farbe setzen
    }

    //Methoden 
    private void placeBombs() {
        Random r = new Random();

        for (int i = 0; i < 10; i++) {

            int x = r.nextInt(10);
            int y = r.nextInt(10);
            if (Msfelder[x][y].isbomb()) {

                i--;
            } else {
                Msfelder[x][y].setIsbomb(true);
            }

        }

    }

    private void einfügenZahlen() {

        for (int z = 0; z < 10; z++) {

            for (int s = 0; s < 10; s++) {

                if (Msfelder[z][s].isbomb() == true) {
                    if (z - 1 >= 0 && s - 1 >= 0 && !Msfelder[z - 1][s - 1].isbomb() == true) { // z-1 s-1
                        Msfelder[z - 1][s - 1].erhoeZahl();

                    }

                    if (z - 1 >= 0 && s < 10 && !Msfelder[z - 1][s].isbomb() == true) { // z-1 s
                        Msfelder[z - 1][s].erhoeZahl();

                    }

                    if (z - 1 >= 0 && s + 1 < 10 && !Msfelder[z - 1][s + 1].isbomb() == true) {// s+1 z-1
                        Msfelder[z - 1][s + 1].erhoeZahl();

                    }

                    if (z < 10 && s - 1 >= 0 && !Msfelder[z][s - 1].isbomb() == true) {// z s-1
                        Msfelder[z][s - 1].erhoeZahl();

                    }

                    if (z < 10 && s + 1 < 10 && !Msfelder[z][s + 1].isbomb() == true) {//z s+1
                        Msfelder[z][s + 1].erhoeZahl();

                    }

                    if (z + 1 < 10 && s - 1 >= 0 && !Msfelder[z + 1][s - 1].isbomb() == true) {// z+1 s-1
                        Msfelder[z + 1][s - 1].erhoeZahl();

                    }

                    if (z + 1 < 10 && s < 10 && !Msfelder[z + 1][s].isbomb() == true) {// z+1 s
                        Msfelder[z + 1][s].erhoeZahl();

                    }

                    if (z + 1 < 10 && s + 1 < 10 && !Msfelder[z + 1][s + 1].isbomb() == true) {// z+1 s+1
                        Msfelder[z + 1][s + 1].erhoeZahl();

                    }

                }

            }// Ende innere FOR

        }// Ende äusere FOR

    }

    private void unCoverButton() {

        for (int z = 0; z < 10; z++) {

            for (int s = 0; s < 10; s++) {
                Msfelder[z][s].show();
            }
        }

    }

    private boolean hasPlayerWon() {
        int count = 0;
        for (int z = 0; z < 10; z++) {
            for (int s = 0; s < 10; s++) {

                if (Msfelder[z][s].isEnabled()) {
                    count++;
                }

            }
        }

        if (count > 10) {
            return false;
        } else {
            return true;
        }
    }

    //Listener
    @Override
    public void mouseClicked(MouseEvent e) {

        mindsweeperbuttonn but = (mindsweeperbuttonn) e.getSource();

        if (but.isbomb() == true) {

            JOptionPane.showMessageDialog(null, "Booooooom!!!");

            for (int z = 0; z < 10; z++) {
                for (int s = 0; s < 10; s++) {
                    try {

                        Msfelder[z][s].setEnabled(false);

                        Thread.sleep(120);

                        this.dispose();
                    } catch (InterruptedException ex) {
                        Logger.getLogger(MSGUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }

        } else if (but.getZahl() >= 1) {
            but.show();
            but.setEnabled(false);

        } else {

            Vector<mindsweeperbuttonn> queue = new Vector<mindsweeperbuttonn>();
            queue.add(but);

            while (!queue.isEmpty()) {

                mindsweeperbuttonn msBut = queue.remove(0);
                if (msBut.getZahl() >= 1) {

                    msBut.show();
                    msBut.setEnabled(false);

                } else {

                    msBut.show();
                    msBut.setEnabled(false);

                    int z = msBut.getposX();
                    int s = msBut.getposY();

                    if (z - 1 >= 0 && s - 1 >= 0 && Msfelder[z - 1][s - 1].isEnabled() && !queue.contains(Msfelder[z - 1][s - 1])) {
                        queue.add(Msfelder[z - 1][s - 1]);
                    }
                    if (z - 1 >= 0 && Msfelder[z - 1][s].isEnabled() && !queue.contains(Msfelder[z - 1][s])) { // z-1 s
                        queue.add(Msfelder[z - 1][s]);

                    }

                    if (z - 1 >= 0 && s + 1 < 10 && Msfelder[z - 1][s + 1].isEnabled() && !queue.contains(Msfelder[z - 1][s + 1])) {// s+1 z-1
                        queue.add(Msfelder[z - 1][s + 1]);

                    }

                    if (s - 1 >= 0 && Msfelder[z][s - 1].isEnabled() && !queue.contains(Msfelder[z][s - 1])) {// z s-1
                        queue.add(Msfelder[z][s - 1]);

                    }

                    if (s + 1 < 10 && Msfelder[z][s + 1].isEnabled() && !queue.contains(Msfelder[z][s + 1])) {//z s+1
                        queue.add(Msfelder[z][s + 1]);

                    }

                    if (z + 1 < 10 && s - 1 >= 0 && Msfelder[z + 1][s - 1].isEnabled() && !queue.contains(Msfelder[z + 1][s - 1])) {// z+1 s-1
                        queue.add(Msfelder[z + 1][s - 1]);

                    }

                    if (z + 1 < 10 && Msfelder[z + 1][s].isEnabled() && !queue.contains(Msfelder[z + 1][s])) {// z+1 s
                        queue.add(Msfelder[z + 1][s]);

                    }

                    if (z + 1 < 10 && s + 1 < 10 && Msfelder[z + 1][s + 1].isEnabled() && !queue.contains(Msfelder[z + 1][s + 1])) {// z+1 s+1
                        queue.add(Msfelder[z + 1][s + 1]);

                    }

                }

            }

        }
        if (hasPlayerWon() == true) {

            try {
                JOptionPane.showMessageDialog(null, "Sie haben gewonnnennn!!!!!!!!!!!!!!!!");

                Thread.sleep(10);

                this.dispose();
            } catch (InterruptedException ex) {
                Logger.getLogger(MSGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

}// ENDE GUI
//
//Msfelder[z-1][s-1].setText("1"); 
//int zahl = Integer.parseInt(Msfelder[z-1][s-1].getText());
//zahl += zahl;
